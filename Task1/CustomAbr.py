import sabre

class CustomAbr(sabre.Abr):
    # def get_quality_delay(self, segment_index):
    #     manifest = self.session.manifest
    #     bitrates = manifest.bitrates
    #     throughput = self.session.get_throughput()
    #     buffers = self.session.get_buffer_contents()
    #     quality = 0
    #     while (quality + 1 < len(bitrates) and
    #            bitrates[quality + 1] <= throughput):
    #         quality += 1
    #     return (quality, 0)
    
    # def get_quality_delay(self, segment_index):
    # # best-speed: stick to lowest bitrate
    #     manifest = self.session.manifest
    #     bitrates = manifest.bitrates
    #     throughput = self.session.get_throughput()
    #     buffers = self.session.get_buffer_contents()
    #     quality = 0
    #     return (quality, 0)
    
    
    def get_quality_delay(self, segment_index):
    # best-quality: stick to highest bitrate
        manifest = self.session.manifest
        bitrates = manifest.bitrates
        throughput = self.session.get_throughput()
        buffers = self.session.get_buffer_contents()
        quality = len(bitrates)-1  
        return (quality, 0)
    
    